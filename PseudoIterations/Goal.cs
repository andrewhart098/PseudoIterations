﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class Goal
    {
        public Guid GoalId { get; set; }
        public string Text { get; set; }
    }
}
