﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class Project
    {
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public string Outline { get; set; }
        public string GitPath { get; set; }
    }
}
