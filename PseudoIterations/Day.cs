﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class Day
    {
        public Guid DayId { get; set; }
        public Guid PseudoIterationId { get; set; }
        public DateTime Date { get; set; }
        public Decimal TotalSpent { get; set; }
    }
}
