﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class ProjectGoals
    {
        public Guid ProjectId { get; set; }
        public Guid GoalId { get; set; }
    }
}
