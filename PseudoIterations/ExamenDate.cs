﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class ExamenDate
    {
        public Guid ExamenId { get; set; }
        public Guid DateId { get; set; }
    }
}
