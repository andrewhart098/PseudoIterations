﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoIterations.Models
{
    public class Examen
    {
        public Guid ExamenId { get; set; }
        public string Text { get; set; }
    }
}
