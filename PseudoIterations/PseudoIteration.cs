﻿using System;

namespace PseudoIterations
{
    public class PseudoIteration
    {
        public Guid PseudoIterationId { get; set; }
        public Guid ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
    }
}
